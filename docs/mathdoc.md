# PhysMath Class

## Inverse square root

```cpp
static float PhysMath::inv_sqrt (float x);
```

```math
f(x) = \frac{1}{\sqrt{x}}
```

$ x^2 $

$$ x^2 $$

`a^2`

$`b^2`$

$$`c^2`$$
